// import express
const express = require('express');
// import controller
const { CreateProductType,
    GetAllProductType,
    GetProductTypeByID,
    UpdateProductType,
    DeleteProductType } = require("../controller/ProductTypeController");
// import middleware
const middlewareApp = require("../../App/middlewares/middleware");
// gán router
const router = express.Router();
// sử dụng middleware
router.use(middlewareApp);
// CreateProductType
router.post("/productType", CreateProductType);
// CreateProductType
router.get("/productType", GetAllProductType);
// CreateProductType
router.get("/productType/:productTypeId", GetProductTypeByID);
// CreateProductType
router.put("/productType/:productTypeId", UpdateProductType);
// CreateProductType
router.delete("/productType/:productTypeId", DeleteProductType);
// export router
module.exports = router;