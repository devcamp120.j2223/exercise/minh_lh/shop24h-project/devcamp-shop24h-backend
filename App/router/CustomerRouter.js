// import express
const express = require('express');
// import controller
const { createCustomer,
    getAllCustomer,
    getCustomerById,
    updateCustomer,
    deleteCustomer } = require("../controller/CustomerController");
// import middleware
const middlewareApp = require("../../App/middlewares/middleware");
// gán router
const router = express.Router();
// sử dụng middleware
router.use(middlewareApp);
// CreateProductType
router.post("/customer", createCustomer);
// CreateProductType
router.get("/customer", getAllCustomer);
// CreateProductType
router.get("/customer/:customerId", getCustomerById);
// CreateProductType
router.put("/customer/:customerId", updateCustomer);
// CreateProductType
router.delete("/customer/:customerId", deleteCustomer);
// export router
module.exports = router;