// import mongoose = require('mongoose');
const mongoose = require('mongoose');
// khỏi tạo shema
const Schema = mongoose.Schema;

const customerModel = new Schema({
    _id: mongoose.Types.ObjectId,
    orderDate: {
        type: Date,
        default: Date.now(),
    },
    shippedDate: {
        type: Date,
        required: true,
    },
    note: {
        type: String
    },
    orderDetail: {
        type: Array,
        required: true,
    },
    cost: {
        type: Number,
        default: 0,
        required: true,
    },
    timeCreated: {
        type: Date,
        default: Date.now()
    },
    timeUpdated: {
        type: Date,
        default: Date.now()
    }
})
module.exports = mongoose.model("orders", customerModel)