// import mongoose = require('mongoose');
const mongoose = require('mongoose');
// khỏi tạo shema
const Schema = mongoose.Schema;

const productModel = new Schema({
    _id: mongoose.Types.ObjectId,
    name: {
        type: String,
        required: true,
        unique: true,
    },
    description: {
        type: String,
    },
    type: {
        type: mongoose.Types.ObjectId,
        ref: "type",
        required: true,
    },
    imageUrl: {
        type: String,
        required: true,
    },
    buyPrice: {
        type: Number,
        required: true,
    },
    promotionPrice: {
        type: Number,
        required: true,
    },
    amount: {
        type: Number,
        default: 0,
        required: true,
    },
    timeCreated: {
        type: Date,
        default: Date.now()
    },
    timeUpdated: {
        type: Date,
        default: Date.now()
    }
})
module.exports = mongoose.model("product", productModel)