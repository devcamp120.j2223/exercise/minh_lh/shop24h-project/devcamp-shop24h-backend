// import mongoose = require('mongoose');
const mongoose = require('mongoose');
// khỏi tạo shema
const Schema = mongoose.Schema;

const customerModel = new Schema({
    _id: mongoose.Types.ObjectId,
    fullName: {
        type: String,
        required: true,
    },
    phone: {
        type: String,
        required: true,
        unique: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    address: {
        type: String,
        defalt: "",
    },
    city: {
        type: String,
        defalt: "",
    },
    country: {
        type: String,
        defalt: "",
    },
    orders: [
        {
            type: mongoose.Types.ObjectId,
            ref: "orders"
        }
    ],
    timeCreated: {
        type: Date,
        default: Date.now()
    },
    timeUpdated: {
        type: Date,
        default: Date.now()
    }
})
module.exports = mongoose.model("customer", customerModel)